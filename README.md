# Cricket Novice Experiments

This repository contains the experiments related to the *Cricket Novice* chatbot. The details of this work are presented in the paper:

**Saurabh Srivastava** and **T.V. Prabhakar**. [Intent Sets - Architectural Choices for Building Practical Chatbot](https://dl.acm.org/doi/10.1145/3384613.3384639). In 2020 12th International Conference on Computer and Automation Engineering (ICCAE), Sydney, Australia. ACM ICPS, 2020

The Bibtex code to cite the paper is:
>@inproceedings{srivastava2020intent,
>  title={Intent Sets: Architectural Choices for Building Practical Chatbots},
>  author={Srivastava, Saurabh and Prabhakar, TV},
>  booktitle={Proceedings of the 2020 12th International Conference on Computer and Automation Engineering},
>  pages={194--199},
>  year={2020}
>}


The Bibtex code to cite this repository is:
>@misc{ssri5-cricketnoviceexperiments,
> author = {Saurabh Srivastava},
> title = {CricketNoviceExperiments Repository — Bitbucket},
> howpublished = {\url{https://bitbucket.org/ssri5/cricketnoviceexperiments/}},
> month = {August},
> year = {2020},
> note = {(Accessed on 08/27/2020)}
>}


The repository contains the following documents:
  - Results of the Accuracy and Order Experiments in documents titled **xxx**-results.data.
  - A josn file called descriptive-intents.json which contains the Examples for training, and static responses for, all the descriptive Intents.
  - A file called Stats.xlsx containing the data used for processing statistical queries

